# bgqmap
Bgqmap it's a python library and a script to parallelize multiple jobs in a DRMAA compatible cluster.

## Install (conda)
```bash
conda install -c conda-forge -c bbglab bgqmap
```

## Install (pip)
```bash
pip install bgqmap
```

## Usage example
```python
from bgqmap import QMapExecutor

# Cluster queues to submit the jobs
queues = ['normal', 'long']

# Maximum jobs running in parallell
max_jobs = 20

# Number of cores that is using each job
cores = 1

# Create the executor
executor = QMapExecutor(
    queues, 
    max_jobs,
    cores,
    output_folder="logs",
    interactive=False,
    adaptative=False,
    commands_per_job=1
)

# Command to run
command = "echo"

# A list or an iterator of all the command arguments to run
arguments = ["Job number {}".format(x) for x in range(20)]

# Run 20 echos in parallell in a DRMAA cluster 
# or using multiprocessing if the cluster is not detected
jobs_done, jobs_fail, jobs_skip = executor.run(
    "echo",
    arguments,
    len(arguments)
)

# Close the executor
executor.exit()
``` 




